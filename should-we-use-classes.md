# Classes and closures

*Should* we use classes? Which one would we prefer to read and
maintain?

## Class

```
class Something {
 constructor(a, b, c) {
  this.a = a
  this.b = b
  this.c = c
 }
 f(d) {
  return this.b(d + this.a);
 }
 g(e) {
  class that = this;
  return () => {
   return that.b(e + that.a);
  }
 }
 ...
}
```

Remember to start class with uppercase, always add `new` when calling it

# Closure

```
function makeSomething(a, b, c) {
 return {
  f(d) {
   return b(d + a);
  },
  g(e) {
   return () => {
    return b(e + a);
   }
 }
}
```

- With strict mode, we get errors about unexisting variables
- There is no way to use this wrong
- Private variables cannot be accessed
- Leads to composition over inheritance
